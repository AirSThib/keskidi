#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QPieSeries>
#include <QChart>
#include <QChartView>
#include <QList>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* WINDOW */

    setWindowTitle("Keskidi");
    setWindowFlags(Qt::Window);

    /* LAYOUT */

    QWidget *mainWidget = new QWidget();
    QGridLayout *mainLayout = new QGridLayout();
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);

    /* TEXT EDIT */

    textEdit = new QTextEdit();
    mainLayout->addWidget(textEdit, 0, 0);

    /* PIE CHART */

    pieChart = new QPieSeries();
    QChart *chart = new QChart();
    chart->addSeries(pieChart);
    chart->setTitle("Results");
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignRight);
    QChartView *chartView = new QChartView(chart);
    mainLayout->addWidget(chartView, 0, 1);

    /* BUTTON */

    QPushButton *analyzeButton = new QPushButton("Analyze");
    connect(analyzeButton, SIGNAL(clicked()), this, SLOT(analyze()));
    mainLayout->addWidget(analyzeButton, 1, 0, 1, 2);
}

void MainWindow::analyze() {
    pieChart->clear();
    QList<QString> langs = {"en", "fr"};
    QString text = " " + textEdit->toPlainText() + " ";
    QStringList words = text.split(" ");
    QList<QStringList> langwords = {{}, {}};
    QList<int> langnote = {0, 0};
    for (int i = 0; i < langs.size(); i++) {
        QFile *langFile = new QFile(qApp->applicationDirPath() + "/" + langs[i] + ".txt");
        langFile->open(QFile::ReadWrite);
        QString langFileContent = QString::fromStdString(langFile->readAll().toStdString());
        QString currentWord;
        for (int j = 0; j < langFileContent.size(); j++) {
            if ((langFileContent[j] == "1" or langFileContent[j] == "2" or langFileContent[j] == "3" or j+1 >= langFileContent.size()) and currentWord != "") {
                langwords[i].append(currentWord);
                currentWord = langFileContent[j];
            }
            
            else if (langFileContent[j] == " ") {
                continue;
            }
            
            else {
                currentWord = currentWord + langFileContent[j];
            }
        }
    }
    
    for (int l = 0; l < words.size(); l++) {
        for (int m = 0; m < langwords.size(); m++) {
            for (int o = 0; o < langwords[m].size(); o++) {
                if (words[l].toLower() == langwords[m][o].mid(1).replace("\n", "").toLower()) {
                    langnote[m] = langnote[m] + langwords[m][o].mid(0, 1).toInt();
                }
            }
        }
    }
    
    for (int p = 0; p < langs.size(); p++) {
        pieChart->append(langs[p], double(langnote[p]));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
